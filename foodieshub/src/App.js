import React from "react";
import { BrowserRouter as Router,Routes,Route } from "react-router-dom";
import { Footer } from "./component/Footer";
import { Navbar } from "./component/Navbar";
import {Home} from "./component/pages/Home";
import {Recipes} from "./component/pages/Recipes";
import {Settings} from "./component/pages/Settings";


function App() {
  return ( 

    <Router>
        <Navbar />
        <div className="container main">
            <Routes>
              <Route path="/" element={<Home/>} />
              <Route path="/recipes" element={<Recipes/>} />
              <Route path="/settings" element={<Settings/>} />
            </Routes>
        </div>
        <Footer />
    </Router>
   
  );
}

export default App;
