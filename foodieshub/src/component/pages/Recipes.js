import React from 'react'
import { PreviousSerches } from '../PreviousSerches'
import { RecipeCard } from '../RecipeCard'

export const Recipes = () => {
  const recipes =[
    {
      title: 'Chicken Pan Pizza',
      image: '/img/gallery/img_1.jpg',
      authorImg: '/img/top-chiefs/img_1.jpg',
      desc:'This cast-iron pizza is the perfect mix of salty, sweet, tangy and sharp. Crispy baked pizza dough is topped with barbecue chicken, mozzarella and cheddar cheese. A drizzle of honey with fresh cilantro and scallion finish off this cheesy chicken pizza.',

    },
    {
      title: 'Spaghetti and Meatballs',
      image: '/img/gallery/img_4.jpg',
      authorImg: '/img/top-chiefs/img_2.jpg',
      desc:'Spaghetti and Meatballs in homemade marinara sauce. These are amazing “WOW”-inducing MEATBALLS so tasty and juicy and BIG!'
    },
    {
      title: 'American Cheese Burger',
      image: '/img/gallery/img_5.jpg',
      authorImg: '/img/top-chiefs/img_3.jpg',
      desc:'This juicy cheeseburger recipe is my twist on the all-American classic recipe. This burger is meaty, cheesy, and extremely juicy.'
    },
    {
      title: 'Mutton Biriyani',
      image: '/img/gallery/img_6.jpg',
      authorImg: '/img/top-chiefs/img_5.jpg',
      desc: 'Mutton biryani is a delicious dish made of meat, spices, herbs & yogurt. Learn to make the best at home with step by step photos.'
    },
    {
      title: 'Japanese Sushi',
      image: '/img/gallery/img_10.jpg',
      authorImg: '/img/top-chiefs/img_6.jpg',
      desc: 'Sushi (寿司 or 鮨) is the most famous Japanese dish outside of Japan, and one of the most popular dishes among the Japanese.'
    },
    {
      title: 'Chicken curry rice',
      image: '/img/gallery/img_3.jpg',
      authorImg: '/img/top-chiefs/img_4.jpg',
      desc:'This easy staple chicken curry is a fantastic recipe for family dinners. Its made with just a handful of ingredients and is enriched with creamy yogurt.'
    },
    {
      title: 'fruits special salad',
      image: '/img/gallery/img_2.jpg',
      authorImg: '/img/top-chiefs/img_2.jpg',
      desc:'Layers of fresh fruit are soaked a citrusy sauce in this colorful salad. ... these simple yet special recipes will make dining in feel like a celebration.'
    }
  ].sort(() => Math.random() - 0.5)
 
  return (
    <div>
    <PreviousSerches />
    <div className='recipes-container'>
        {/* <RecipeCard /> */}
        {recipes.map((recipe,index) => (
            <RecipeCard key={index} recipe={recipe} />
        ))}
    </div>
    </div>
  )
}
