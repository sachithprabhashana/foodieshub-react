import React from 'react'
import { ChiefsSection } from '../ChiefsSection'
import { HeroSection } from '../HeroSection'
import { ImproveSkills } from '../ImproveSkills'
import { QuoteSection } from '../QuoteSection'

export const Home = () => {
  return (
    <div>
    <HeroSection />
    <ImproveSkills />
    <QuoteSection />
    <ChiefsSection />
    </div>
  )
}
