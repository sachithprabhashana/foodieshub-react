import React from 'react'
import { CustomImage } from './CustomImage'

export const HeroSection = () => {

  const images = [
    "/img/gallery/img_1.jpg",
    "/img/gallery/img_2.jpg",
    "/img/gallery/img_3.jpg",
    "/img/gallery/img_4.jpg",
    "/img/gallery/img_5.jpg",
    "/img/gallery/img_6.jpg",
    "/img/gallery/img_7.jpg",
    "/img/gallery/img_8.jpg",
    "/img/gallery/img_9.jpg",

  ]

  return (
    <div className='section hero'>
        <div className='col typography'>
            <h className='title'>What Are We About</h>
            <p className='info'>FoodieHub is a place where you can please you soul and 
            tummy with delicious food recepies of all cuisine and our service is
            absolutely free.So start exploring now
            </p>
            <button className='btn'>Explore now</button>
        </div>
        <div className='col gallery'>
          { images.map((src, index) => (
             <CustomImage key={index} imgSrc={src} pt={'90%'} />
          ))}
         

        </div>
    </div>
  )
}
