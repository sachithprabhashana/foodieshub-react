import React from 'react'
import { ChiefsCard } from './ChiefsCard'

export const ChiefsSection = () => {
  const chiefs = [
    {
      name: 'kean silvester',
      img: '/img/top-chiefs/img_1.jpg',
      recipesCount: '12',
      cusine:'Maxican',
    },
    {
      name: 'don sherman',
      img: '/img/top-chiefs/img_2.jpg',
      recipesCount: '5',
      cusine:'Srilankan',
    },
    {
      name: 'haward lose',
      img: '/img/top-chiefs/img_3.jpg',
      recipesCount: '20',
      cusine:'England',
    },
    {
      name: 'Harry martin',
      img: '/img/top-chiefs/img_4.jpg',
      recipesCount: '9',
      cusine:'Australian',
    },
    {
      name: 'viley holi',
      img: '/img/top-chiefs/img_5.jpg',
      recipesCount: '14',
      cusine:'america',
    },
    {
      name: 'alfa richmand',
      img: '/img/top-chiefs/img_6.jpg',
      recipesCount: '10',
      cusine:'india',
    },
  ]
  return (
    <div className='section chiefs'>
      <h1 className='title'>Our Top Cheifs</h1>
      <div className='top-chiefs-container'>
        {/* <ChiefsCard />
        <ChiefsCard />
        <ChiefsCard />
        <ChiefsCard />
        <ChiefsCard />
        <ChiefsCard /> */}

        {chiefs.map(chief => <ChiefsCard key={chief.name} chief={chief} /> )}
      </div>
    </div>
  )
}
