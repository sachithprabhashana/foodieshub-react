import React from 'react'

export const ImproveSkills = () => {

  const list = [
    "Learn new recipies",
    "Experiment with food",
    "Write your own recipies",
    "Know nutrition facts",
    "Get cooking tips",
    "Get ranked",
  ]
  return (
    <div className='section improve-skills'>
    <div className='col img'>
        <img src='/img/gallery/img_10.jpg' alt=''/>
</div>

    <div className='col typography'>
        <h className='title'>Improve Your Culinary Skills</h>
        {list.map((item,index) => (
          <p className='skill-item' key={index}>{item}</p>
        ))}
        <button className='btn'>Signup now</button>
    </div>
</div>
  )
}
 