import React from "react";
import { FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faQuoteLeft } from "@fortawesome/free-solid-svg-icons";


export const QuoteSection = () => {
    return(
        <div className="section quote">
           <p className="quote-text"><FontAwesomeIcon icon={faQuoteLeft}/> food is everythings we are.]
           It's an extention of nationalist feeling,
           ethnic feeling,your personal history.your province,
           your region,your tribe,your grandma.It's inseparable from 
           those fron the get go.
           </p>
           <p className="quote-author">
            -Anthony Bourdain
           </p>
        </div>
    )
}