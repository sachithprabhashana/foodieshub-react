import React, { useState } from 'react';
import {Link,useLocation} from "react-router-dom";
import { Sidebar } from './Sidebar';
import {faHome,faList,faCog} from '@fortawesome/free-solid-svg-icons';

export const Navbar = () => {

const [showsidebar,setShowsidebar] = useState(false);
const location = useLocation();
const links = [
  {
    name: 'Home',
    path: '/',
    icon: faHome
  },
  {
    name: 'Recipes',
    path: '/recipes',
    icon: faList
  },
  {
    name: 'Settings',
    path: '/settings',
    icon: faCog
  },
]

function closeSidebar() {
  setShowsidebar(false);
}

  return (
    <>
    <div className='navbar container'>
        <Link to="/" className='logo'>F<span>oo</span>diesHub</Link>
        <div className='nav-links'>
      {links.map(link => (
        <Link className= {location.pathname === link.path ? "active" : ""} to={link.path} href='#!' key={link.name}>{link.name}</Link>
      ))}
            {/* <a href='#!'>Home</a>
            <a href='#!'>Recipes</a>
            <a href='#!'>Settings</a> */}
        </div>
        <div className= {showsidebar ? 'sidebar-btn active' : 'sidebar-btn'} onClick={() => setShowsidebar(true) }>
           <div className='bar'></div>
           <div className='bar'></div>
           <div className='bar'></div>
          
        </div>
    </div>
    {showsidebar && <Sidebar close={closeSidebar} links={links} />  }

    </>
  )     
}
   