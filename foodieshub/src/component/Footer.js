import React from 'react'

export const Footer = () => {
  return (
    <div className='footer container'>
    <div className='footer-section'>
        <p className='title'>FoodiesHub.com</p>
        <p>FoodiesHub is a place where you can please your soul and tummy with delicious food 
            recipes af all cusine.And our service is absolutely free.
             </p>
        <p>&copy;2021 | All Rights Reserved</p>
    </div>
    <div className='footer-section'>
        <p className='title'>Contact Us</p>
        <p>Foodieshub@gmail.com</p>
        <p>+94-113455644</p>
        <p>1222 street NYC</p>
    </div>  
    <div className='footer-section'>
        <p className='title'>Socials</p>
        <p>Facebook</p>
        <p>Twitter</p>
        <p>Instagram</p>
    </div>

    </div>
    
  )
}
 